package io.muzoo.ooc.ecosystems.hunter;

import io.muzoo.ooc.ecosystems.actor.Actor;
import io.muzoo.ooc.ecosystems.animal.Animal;
import io.muzoo.ooc.ecosystems.field.Field;
import io.muzoo.ooc.ecosystems.field.Location;

import java.util.Iterator;
import java.util.List;

public class Hunter extends Actor {

    private Location location;
    private boolean active;
    private int age;

    public Hunter(boolean randomAge) {
        active = true;
    }

    @Override
    public void act(Field currentField, Field updatedField, List<Actor> newActors) {
        Location newLocation = hunt(currentField, getLocation());
        if (newLocation == null) {
            newLocation = updatedField.freeAdjacentLocation(getLocation());
        }
        if (newLocation != null) {
            setLocation(newLocation);
            updatedField.place(this, newLocation);
        } else {
            // can neither move nor stay - overcrowding - all locations taken
            setInactive();
        }
    }

    public Location hunt(Field field, Location location) {
        Iterator adjacentLocations = field.adjacentLocations(location);

        while (adjacentLocations.hasNext()) {
            Location where = (Location) adjacentLocations.next();
            Object unknown = field.getObjectAt(where);
            if (unknown instanceof Animal) {
                Animal animal = (Animal) unknown;
                animal.setInactive();
                return where;
            }
        }

        return null;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(int row, int col) {
        location = new Location(row, col);
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setInactive() {
        active = false;
    }
}
