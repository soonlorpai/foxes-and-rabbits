package io.muzoo.ooc.ecosystems.simulator;

import io.muzoo.ooc.ecosystems.actor.Actor;
import io.muzoo.ooc.ecosystems.actor.ActorFactory;
import io.muzoo.ooc.ecosystems.animal.*;
import io.muzoo.ooc.ecosystems.field.Field;
import io.muzoo.ooc.ecosystems.hunter.Hunter;

import java.util.*;
import java.awt.Color;

/**
 * A simple predator-prey simulator, based on a field containing
 * rabbits and foxes.
 *
 * @author David J. Barnes and Michael Kolling
 * @version 2002.10.28
 */
public class Simulator {

    private static final int DEFAULT_WIDTH = 50;
    private static final int DEFAULT_DEPTH = 50;

    private List actors;
    private List newActors;
    private Field field;
    private Field updatedField;
    private int step;
    private SimulatorView view;

    /**
     * Construct a simulation field with default size.
     */
    public Simulator() {
        this(DEFAULT_DEPTH, DEFAULT_WIDTH);
    }

    /**
     * Create a simulation field with the given size.
     *
     * @param depth Depth of the field. Must be greater than zero.
     * @param width Width of the field. Must be greater than zero.
     */
    public Simulator(int depth, int width) {
        if (width <= 0 || depth <= 0) {
            System.out.println("The dimensions must be greater than zero.");
            System.out.println("Using default values.");
            depth = DEFAULT_DEPTH;
            width = DEFAULT_WIDTH;
        }
        actors = new ArrayList();
        newActors = new ArrayList();
        field = new Field(depth, width);
        updatedField = new Field(depth, width);

        view = new SimulatorView(depth, width);
        view.setColor(Fox.class, Color.blue);
        view.setColor(Rabbit.class, Color.orange);
        view.setColor(Tiger.class, Color.black);
        view.setColor(Hunter.class, Color.green);

        reset();
    }

    /**
     * Run the simulation from its current state for a reasonably long period,
     * e.g. 500 steps.
     */
    public void runLongSimulation() {
        simulate(500);
    }

    /**
     * Run the simulation from its current state for the given number of steps.
     * Stop before the given number of steps if it ceases to be viable.
     */
    public void simulate(int numSteps) {
        for (int step = 1; step <= numSteps && view.isViable(field); step++) {
            simulateOneStep();
        }
    }

    /**
     * Run the simulation from its current state for a single step.
     * Iterate over the whole field updating the state of each
     * tiger, fox, and rabbit.
     */
    public void simulateOneStep() {
        step++;
        newActors.clear();

        for (Iterator<Actor> iter = actors.iterator(); iter.hasNext(); ) {
            Actor actor = iter.next();
            actor.act(field, updatedField, newActors);

            if(!actor.isActive()) iter.remove();
        }

        actors.addAll(newActors);

        // Swap the field and updatedField at the end of the step.
        Field temp = field;
        field = updatedField;
        updatedField = temp;
        updatedField.clear();

        // display the new field on screen
        view.showStatus(step, field);
    }

    /**
     * Reset the simulation to a starting position.
     */
    public void reset() {
        step = 0;
        actors.clear();
        field.clear();
        updatedField.clear();
        populate(field);

        // Show the starting state in the view.
        view.showStatus(step, field);
    }

    /**
     * Populate a field with foxes and rabbits.
     *
     * @param field The field to be populated.
     */
    private void populate(Field field) {
        ActorFactory actorFactory = new ActorFactory();
        field.clear();
        for (int row = 0; row < field.getDepth(); row++) {
            for (int col = 0; col < field.getWidth(); col++) {
                Actor actor = actorFactory.createRandomActor(row, col, field);
                if (actor != null) actors.add(actor);
            }
        }
        Collections.shuffle(actors);
    }
}
