package io.muzoo.ooc.ecosystems.actor;

import io.muzoo.ooc.ecosystems.animal.Fox;
import io.muzoo.ooc.ecosystems.animal.Rabbit;
import io.muzoo.ooc.ecosystems.animal.Tiger;
import io.muzoo.ooc.ecosystems.field.Field;
import io.muzoo.ooc.ecosystems.field.Location;
import io.muzoo.ooc.ecosystems.hunter.Hunter;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public class ActorFactory {

    private final Map<ActorType, Double> creationProbabilities = new LinkedHashMap<ActorType, Double>() {{
        put(ActorType.HUNTER, 0.05);
        put(ActorType.TIGER, 0.05);
        put(ActorType.FOX, 0.1);
        put(ActorType.RABBIT, 0.2);
    }};
    private Random rand = new Random();

    public Actor createRandomActor(int row, int col, Field field) {

        double random = rand.nextDouble();
        for (Map.Entry<ActorType, Double> entry : creationProbabilities.entrySet()) {
            ActorType type = entry.getKey();
            Double prob = entry.getValue();
            if (random < prob) {
                Actor actor = instantiate(type, true);
                actor.setLocation(row, col);
                field.place(actor, row, col);
                return actor;

            } else {
                random = random - prob;
            }
        }

        return null;
    }

    public Actor createNewActor(ActorType actorType, Field field, Location location) {
        Actor actor = instantiate(actorType, false);
        actor.setLocation(location);
        field.place(actor, location);
        return actor;
    }

    public Actor instantiate(ActorType actorType, boolean randomAge) {
        switch (actorType) {
            case HUNTER: return new Hunter(randomAge);
            case FOX: return new Fox(randomAge);
            case RABBIT: return new Rabbit(randomAge);
            case TIGER: return new Tiger(randomAge);
            default: return null;
        }
    }
}
