package io.muzoo.ooc.ecosystems.actor;

public enum ActorType {
    HUNTER, RABBIT, FOX, TIGER
}
