package io.muzoo.ooc.ecosystems.actor;

import io.muzoo.ooc.ecosystems.field.Field;
import io.muzoo.ooc.ecosystems.field.Location;

import java.util.List;

public abstract class Actor {

    abstract public void act(Field currentField, Field updatedField, List<Actor> newActors);
    abstract public boolean isActive();
    abstract public void setInactive();
    abstract public void setLocation(int row, int col);
    abstract public void setLocation(Location location);

}
