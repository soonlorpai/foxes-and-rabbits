package io.muzoo.ooc.ecosystems.animal;

import io.muzoo.ooc.ecosystems.actor.Actor;
import io.muzoo.ooc.ecosystems.field.Field;
import io.muzoo.ooc.ecosystems.field.Location;

import java.util.List;
import java.util.Random;

public abstract class Animal extends Actor {

    // Individual characteristics (instance fields).
    // Whether the animal is alive or not.
    private boolean alive;
    // The animal's position
    private Location location;
    // The animal's age
    private int age;

    // Characteristics shared by all animals (static fields).
    abstract protected int getBreedingAge();
    abstract protected int getMaxAge();
    abstract protected int getMaxLitterSize();
    abstract protected double getBreedingProbability();
    // A shared random number generator to control breeding.
    protected static final Random rand = new Random();


    protected Animal(boolean randomAge) {
        alive = true;
        age = 0;
        if (randomAge) {
            age = rand.nextInt(getMaxAge());
        }
    }

    public abstract void act(Field currentField, Field updatedField, List newAnimal);

    /**
     * Increase the age. This could result in the animal's death.
     */
    protected void incrementAge() {
        age++;
        if (age > getMaxAge()) {
            setInactive();
        }
    }

    /**
     * An animal can breed if it has reached the breeding age.
     */
    public boolean canBreed() {
        return age >= getBreedingAge();
    }

    /**
     * Generate a number representing the number of births,
     * if it can breed.
     *
     * @return The number of births (may be zero).
     */
    protected int breed() {
        int births = 0;
        if (canBreed() && (rand.nextDouble() <= getBreedingProbability()) ) {
            births = rand.nextInt(getMaxLitterSize()) + 1;
        }
        return births;
    }

    public void move(Field currentField, Field updatedField) {
        // Move towards the source of food if found.
        Location newLocation = findFood(currentField, getLocation());
        if (newLocation == null) {  // no food found - move randomly
            newLocation = updatedField.freeAdjacentLocation(getLocation());
        }
        else if (newLocation != null) {
            setLocation(newLocation);
            updatedField.place(this, newLocation);
        } else {
            // can neither move nor stay - overcrowding - all locations taken
            setInactive();
        }
    }

    protected abstract Location findFood(Field currentField, Location location);

    /**
     * Check whether the animal is alive or not.
     *
     * @return True if the animal is still alive.
     */
    public boolean isActive() {
        return alive;
    }

    /**
     * Animal is no longer alive.
     *
     */
    public void setInactive() { alive = false; }

    /**
     * @return The animal's location.
     */
    public Location getLocation() {
        return location;
    }

    /**
     * Set the animal's location.
     *
     * @param row The vertical coordinate of the location.
     * @param col The horizontal coordinate of the location.
     */
    public void setLocation(int row, int col) {
        this.location = new Location(row, col);
    }

    /**
     * Set the animal's location.
     *
     * @param location The animal's location.
     */
    public void setLocation(Location location) {
        this.location = location;
    }

}
