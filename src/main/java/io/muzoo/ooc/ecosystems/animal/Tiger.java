package io.muzoo.ooc.ecosystems.animal;

import io.muzoo.ooc.ecosystems.field.Field;
import io.muzoo.ooc.ecosystems.field.Location;

import java.util.Iterator;
import java.util.List;

public class Tiger extends Animal{

    private static final int BREEDING_AGE = 20;
    private static final int MAX_AGE = 200;
    private static final double BREEDING_PROBABILITY = 0.09;
    private static final int MAX_LITTER_SIZE = 5;
    private static final int RABBIT_FOOD_VALUE = 20;
    private static final int FOX_FOOD_VALUE = 40;
    private static final double FOX_EATING_PROBABILITY = 0.5;

    private int foodLevel;

    public Tiger(boolean randomAge) {
        super(randomAge);
        if (randomAge) {
            foodLevel = rand.nextInt(RABBIT_FOOD_VALUE);
        } else {
            foodLevel = RABBIT_FOOD_VALUE;
        }
    }

    /**
     * This is what the tiger does most of the time: it hunts for
     * rabbits and foxes. In the process, it might breed, die of hunger,
     * or die of old age.
     *
     * @param currentField The field currently occupied.
     * @param updatedField The field to transfer to.
     * @param newTigers     A list to add newly born foxes to.
     */
    @Override
    public void act(Field currentField, Field updatedField, List newTigers) {
        incrementAge();
        incrementHunger();
        if (isActive()) {
            int births = breed();
            for (int b = 0; b < births; b++) {
                Tiger newTiger = new Tiger(false);
                newTigers.add(newTiger);
                Location loc = updatedField.randomAdjacentLocation(getLocation());
                newTiger.setLocation(loc);
                updatedField.place(newTiger, loc);
            }

            move(currentField, updatedField);
        }
    }

    @Override
    protected Location findFood(Field field, Location location) {
        Iterator adjacentLocations = field.adjacentLocations(location);

        while (adjacentLocations.hasNext()) {
            Location where = (Location) adjacentLocations.next();
            Object animal = field.getObjectAt(where);
            if (animal instanceof Fox) {
                Fox fox = (Fox) animal;
                if (fox.isActive() && rand.nextDouble() < FOX_EATING_PROBABILITY) {
                    fox.setInactive();
                    if (foodLevel < FOX_FOOD_VALUE)
                        foodLevel = FOX_FOOD_VALUE;
                    else if (foodLevel < RABBIT_FOOD_VALUE + FOX_FOOD_VALUE)
                        foodLevel += FOX_FOOD_VALUE;
                    else foodLevel = FOX_FOOD_VALUE;
                    return where;
                }
            }
            if (animal instanceof Rabbit) {
                Rabbit rabbit = (Rabbit) animal;
                if (rabbit.isActive()) {
                    rabbit.setInactive();
                    if (foodLevel < RABBIT_FOOD_VALUE)
                        foodLevel = RABBIT_FOOD_VALUE;
                    else if (foodLevel < RABBIT_FOOD_VALUE + FOX_FOOD_VALUE)
                        foodLevel += RABBIT_FOOD_VALUE;
                    else foodLevel = RABBIT_FOOD_VALUE;
                    return where;
                }
            }
        }
        return null;
    }

    private void incrementHunger() {
        foodLevel--;
        if (foodLevel <= 0) {
            setInactive();
        }
    }

    @Override
    protected int getBreedingAge() {
        return BREEDING_AGE;
    }

    @Override
    protected int getMaxAge() {
        return MAX_AGE;
    }

    @Override
    protected int getMaxLitterSize() {
        return MAX_LITTER_SIZE;
    }

    @Override
    protected double getBreedingProbability() {
        return BREEDING_PROBABILITY;
    }
}
