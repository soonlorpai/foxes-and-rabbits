package io.muzoo.ooc.ecosystems;

import io.muzoo.ooc.ecosystems.simulator.Simulator;

public class Application {
    public static void main(String[] args) {
        Simulator sim = new Simulator(100,50);
        sim.simulate(100);
    }

}
